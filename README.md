# goto node modules

[![Marketplace Version](https://vsmarketplacebadge.apphb.com/version/ravenq.vscode-goto-node-modules.svg)](https://marketplace.visualstudio.com/items?itemName=ravenq.vscode-goto-node-modules) [![Installs](https://vsmarketplacebadge.apphb.com/installs/ravenq.vscode-goto-node-modules.svg)](https://marketplace.visualstudio.com/items?itemName=ravenq.vscode-goto-node-modules) [![Rating](https://vsmarketplacebadge.apphb.com/rating/ravenq.vscode-goto-node-modules.svg)](https://marketplace.visualstudio.com/items?itemName=ravenq.vscode-goto-node-modules) [![Build Status](https://travis-ci.org/ravenq/vscode-goto-node-modules.svg?branch=master)](https://travis-ci.org/ravenq/vscode-goto-node-modules)

goto npm module/main from vue/js code.

## usage

ctrl + cursor goto node modules.

![usage](https://gitee.com/binote/vscode-goto-node-modules/raw/master/doc/usage.gif)

## 原作者为ravenq

fork自：

https://github.com/ravenq/vscode-goto-node-modules

## 新改动

支持优先跳转package里面声明的module字段入口，如果不存在，跳转main入口，如果还是不存在，再跳转到package.josn